import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { HomePage } from "./home.page";
import {
  AngularFireAnalyticsModule,
  AngularFireAnalytics,
  DEBUG_MODE,
} from "@angular/fire/analytics";
import { initializeApp } from "firebase";
import { AngularFireModule } from "@angular/fire";
import { environment } from "../../environments/environment";

describe("HomePage", () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomePage],
      imports: [
        IonicModule.forRoot(),
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireAnalyticsModule,
      ],
      providers: [{ provide: DEBUG_MODE, useValue: true }],
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
