// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDCBzWhnAKTHWkRUyEucax3Fqm9OoM3Cws",
    authDomain: "qr-scanner-mihurasoft.firebaseapp.com",
    databaseURL: "https://qr-scanner-mihurasoft.firebaseio.com",
    projectId: "qr-scanner-mihurasoft",
    storageBucket: "qr-scanner-mihurasoft.appspot.com",
    messagingSenderId: "612762876575",
    appId: "1:612762876575:web:a1d474fbaec79c7f2300aa",
    measurementId: "G-Q9RMFWZM64",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
