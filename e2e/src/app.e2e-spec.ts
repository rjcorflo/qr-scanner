import { AppPage } from "./app.po";

describe("new App", () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it("should contan App name", () => {
    page.navigateTo();
    expect(page.getParagraphText()).toContain("Escaneador QR");
  });
});
